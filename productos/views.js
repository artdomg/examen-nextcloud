(function(namespace) {

  var views = namespace.views;
  var collections = namespace.collections;
  var models = namespace.models;

  var getTemplate = function(name) {
    return hbs.compile($('#productos-' + name + '-template').html());
  }

  views.Main = Bb.View.extend({
    template: getTemplate('main'),

    events: {
      'click #btnNuevo': 'onNuevoProducto',
    },

    initialize: function() {
      var me = this;
      me.productos = new collections.Productos();
      me.render();

      me.mostrarLista();
    },

    render: function() {
      var me = this;
      me.$el.html(me.template());
      return me;
    },

    onNuevoProducto: function(e){
      var me = this;
      if(me.nuevoproducto != null)
        me.nuevoproducto.undelegateEvents();

      me.nuevoproducto = new views.Nuevo({
        el: me.$("#resultados"),
        collection: me.productos,
        padre: me
      });
    },

    mostrarLista: function(){
      var me = this;
      if(me.lista != null)
        me.lista.undelegateEvents();

      me.lista = new views.Lista({
        el: me.$('#resultados'),
        collection: me.productos,
        padre: me
      });
    },

    editar: function(producto){
      var me = this;
      if(me.edicion != null)
        me.edicion.undelegateEvents();

      me.edicion = new views.Editar({
        el: me.$("#resultados"),
        collection: producto,
        padre: me
      });
    },

  });

  views.Lista = Bb.View.extend({
    template: getTemplate('lista'),

    events: {
      'click .selectable': 'onEditar',
    },

    initialize: function(){
      var me = this;
      me.render();
    },
    render: function(){
      var me = this;
      me.$el.html(me.template({
        productos: me.collection.toJSON()
      }));
      return me;
    },
    onEditar: function(e){
      var me = this;
      var id = e.currentTarget.id.substring(2);
      var producto = me.collection.get({id: id});
      me.options.padre.editar(producto);
    }
  });

  views.Nuevo = Bb.View.extend({
    template: getTemplate('nuevo'),

    events: {
      'click #btnGuardar': 'onGuardar',
      'click #btnCancelar': 'onCancelar',
    },

    initialize: function(){
      var me = this;
      me.render();
    },
    render: function(){
      var me = this;
      me.$el.html(me.template());
      return me;
    },
    onGuardar: function(e){
      var me = this;

      if($("#id").val().length > 0 && $("#nombre").val().length > 0 && $("#precio").val().length > 0){
        me.collection.add({
          id: $("#id").val(),
          nombre: $("#nombre").val(),
          precio: $("#precio").val()
        });

        me.options.padre.mostrarLista();
      }
      else
        alert("Datos obligatorios");
    },
    onCancelar: function(e){
      var me = this;
      me.options.padre.mostrarLista();
    }
  });

  views.Editar = Bb.View.extend({
    template: getTemplate('editar'),

    events: {
      'click #btnGuardarEdicion': 'onGuardarEdicion',
      'click #btnCancelarEdicion': 'onCancelarEdicion',
      'click #btnEliminar': 'onEliminar',
    },

    initialize: function(){
      var me = this;
      me.render();
    },
    render: function(){
      var me = this;
      me.$el.html(me.template({
        producto: me.collection.toJSON()
      }));
      return me;
    },
    onGuardarEdicion: function(e){
      var me = this;

      if($("#id").val().length > 0 && $("#nombre").val().length > 0 && $("#precio").val().length > 0){
        me.collection.set({
          id: $("#id").val(),
          nombre: $("#nombre").val(),
          precio: $("#precio").val()
        });

        me.options.padre.productos.set([me.collection], {remove: false});
        me.options.padre.mostrarLista();
      }
      else
        alert("Datos obligatorios");
    },
    onCancelarEdicion: function(e){
      var me = this;
      me.options.padre.mostrarLista();
    },
    onEliminar: function(e){
      var me = this;
      me.options.padre.productos.remove(me.collection);
      me.options.padre.mostrarLista();
    }
  });

})(productos);